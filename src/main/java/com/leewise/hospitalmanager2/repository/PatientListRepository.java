package com.leewise.hospitalmanager2.repository;

import com.leewise.hospitalmanager2.entity.PatientsList;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatientListRepository extends JpaRepository<PatientsList,Long> {

}
