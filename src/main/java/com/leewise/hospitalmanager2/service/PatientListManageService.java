package com.leewise.hospitalmanager2.service;

import com.leewise.hospitalmanager2.entity.PatientsList;
import com.leewise.hospitalmanager2.model.PatientListRequest;
import com.leewise.hospitalmanager2.repository.PatientListRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PatientListManageService {
    private final PatientListRepository patientListRepository;
    public void setPatientList(PatientListRequest request) {
        PatientsList addData = new PatientsList.PatentsListBuilder(request).build();
        patientListRepository.save(addData);

    }
}
