package com.leewise.hospitalmanager2.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}

