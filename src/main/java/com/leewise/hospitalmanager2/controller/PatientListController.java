package com.leewise.hospitalmanager2.controller;

import com.leewise.hospitalmanager2.model.PatientListRequest;
import com.leewise.hospitalmanager2.service.PatientListManageService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/patient")
public class PatientListController {
    private final PatientListManageService patientListManageService;
    @PostMapping("/add-list")
    public String setPatientList(PatientListRequest request) {
        patientListManageService.setPatientList(request);
        return  "thanks";
    }


}
