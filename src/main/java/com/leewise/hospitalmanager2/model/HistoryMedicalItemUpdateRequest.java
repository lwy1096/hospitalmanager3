package com.leewise.hospitalmanager2.model;

import com.leewise.hospitalmanager2.enums.TreatmentList;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HistoryMedicalItemUpdateRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private TreatmentList treatmentList;

}
