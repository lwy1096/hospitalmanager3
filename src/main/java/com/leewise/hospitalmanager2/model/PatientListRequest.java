package com.leewise.hospitalmanager2.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter


public class PatientListRequest {
    @NotNull
    private String patientName;
    @NotNull
    private String idNumber;
    @NotNull
    private String phoneNum;
    @NotNull
    private String address;
    @NotNull
    private String purpose;
    @NotNull
    private LocalDate enrollDate;
}
