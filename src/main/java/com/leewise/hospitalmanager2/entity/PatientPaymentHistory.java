package com.leewise.hospitalmanager2.entity;

import com.leewise.hospitalmanager2.enums.TreatmentList;
import com.leewise.hospitalmanager2.interfaces.CommonModelBuilder;
import com.leewise.hospitalmanager2.model.HistoryMedicalItemUpdateRequest;
import com.leewise.hospitalmanager2.model.HistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PatientPaymentHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @JoinColumn(name = "PatientID",nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private PatientsList patentsList;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private TreatmentList treatmentList;

    @Column(nullable = false)
    private Integer price;

    @Column(nullable = false)
    private Boolean isInsuranced;

    @Column(nullable = false)
    private LocalTime treatmentTime;
    @Column(nullable = false)
    private LocalDate treatmentDate;
    @Column(nullable = false)
    private Boolean isPaid ;
    public void putCompletePayment() {

        this.isPaid = true;
    }
    public void putTreatment(HistoryMedicalItemUpdateRequest updateRequest) {
        this.treatmentList = updateRequest.getTreatmentList();
    }
  private PatientPaymentHistory(PatientPaymentHistoryBuilder builder) {
        this.treatmentList = builder.treatmentList;
        this.patentsList = builder.patentsList;
        this.price = builder.price;
        this.isInsuranced = builder.isInsuranced;
        this.treatmentTime = builder.treatmentTime;
        this.treatmentDate = builder.treatmentDate;
        this.isPaid = builder.isPaid;

    }
    public static class PatientPaymentHistoryBuilder implements CommonModelBuilder<PatientPaymentHistory> {

        private final PatientsList patentsList;
        private final TreatmentList treatmentList;
        private final Integer price;
        private final Boolean isInsuranced;
        private final LocalTime treatmentTime;
        private final LocalDate treatmentDate;
        private final  Boolean isPaid ;

        public PatientPaymentHistoryBuilder(PatientsList patientsList, HistoryRequest request) {
        this.patentsList = patientsList;
        this.treatmentList = request.getTreatmentList();
        this.isInsuranced = request.getIsInsuranced();
        this.price = request.getIsInsuranced() ? request.getTreatmentList().getInsurancedPrice() : request.getTreatmentList().getNonInsurancedPrice();
        this.treatmentTime = LocalTime.now();
        this.treatmentDate = LocalDate.now();
        this.isPaid = false;
        }


        @Override
        public PatientPaymentHistory build() {
            return new PatientPaymentHistory(this);
        }
    }
}
