package com.leewise.hospitalmanager2.entity;

import com.leewise.hospitalmanager2.interfaces.CommonModelBuilder;
import com.leewise.hospitalmanager2.model.PatientListRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PatientsList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false,length = 15)
    private String patientName;

    @Column(nullable = false, length = 14)
    private String idNumber;

    @Column(nullable = false,length = 13)
    private String phoneNum;
    @Column(nullable = false)
    private String address;

    @Column(nullable = false , columnDefinition = "TEXT")
    private String purpose;
    @Column(nullable = false)
    private LocalDate enrollDate;

    public PatientsList(PatentsListBuilder builder){
        this.patientName = builder.patientName;
        this.idNumber = builder.idNumber;
        this.phoneNum = builder.phoneNum;
        this.address = builder.address;
        this.purpose = builder.purpose;
        this.enrollDate = builder.enrollDate;
    }
    public static class PatentsListBuilder implements CommonModelBuilder<PatientsList> {
        private final String patientName;
        private final String idNumber;
        private final String phoneNum;
        private final String address;
        private final String purpose;
        private final LocalDate enrollDate;
        public PatentsListBuilder(PatientListRequest request) {
            this.patientName = request.getPatientName();
            this.idNumber = request.getIdNumber();
            this.phoneNum = request.getPhoneNum();
            this.address = request.getAddress();
            this.purpose = request.getPurpose();
            this.enrollDate = LocalDate.now();
        }

        @Override
        public PatientsList build() {
            return new PatientsList(this);
        }
    }

}
